import React, { Component } from 'react';

class Footer extends Component {

  render() {
    const {company, email} = this.props;
    return (
      <div className="container-fluid">
        <hr />
        <div className="text-center title text-uppercase mb-3">
          <small>
            <span className="text-danger">This is {company}</span> | <span className="text-muted">Contact by {email}</span>
          </small>
        </div>
      </div>
    )
  }
}

export default Footer;
