import React, { Component } from 'react';

class ProductItem extends Component {

  constructor(props) {
    super(props);
    console.log('constructor|' + props.productName)
  }

  doSomething(e) {
    console.log(e.target.value)
  }

  render() {
    const {productName, unitPrice, thumbnail} = this.props.product;
    return (
      <div className="col-md-3 col-sm-6">
        <img className="img-fluid img-thumbnail" src={thumbnail} alt="products" />
        <h5 className="mt-2">{productName}</h5>
        <p className="title text-right">{unitPrice} THB</p>
        {/* <button className="btn btn-block btn-secondary title" value={unitPrice} onClick={(e) => this.doSomething(e)}>
          Buy
        </button> */}
        {this.props.onAddOrder &&
				<button className="btn btn-block btn-secondary title" onClick={() => this.props.onAddOrder(this.props.product)}>
          Buy
        </button>
        }

        {(this.props.onDelProduct || this.props.onEditProduct) &&
        <button className="btn btn-info col-md-5 title" onClick={() => this.props.onEditProduct(this.props.product)}>
          Edit
        </button>
        }

        {(this.props.onDelProduct || this.props.onEditProduct) &&
        <button className="btn btn-danger col-md-5 float-right title" onClick={() => this.props.onDelProduct(this.props.product)}>
          Delete
        </button>
        }

        <hr />
      </div>
    )
  }
}

export default ProductItem;
