import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import reduxThunk from 'redux-thunk';
import reducers from "./reducers/index";

const store = createStore(reducers, {}, applyMiddleware(reduxThunk))

// let user = {username : "jayudev", email : "jayudevelop@gmail.com"}
// const element = <h1>Hello {user.username + " " + user.email}</h1>

// ReactDOM.render(element, document.getElementById('root'));
ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>
  , document.getElementById('root'));
