// Export all module
export * from "./OrderActions";
export * from "./ProductActions";

// *Export multi module
// export { ordersFetch, orderDelete } from "./OrderActions";
// export { productsFetch, productDelete } from "./ProductActions";
