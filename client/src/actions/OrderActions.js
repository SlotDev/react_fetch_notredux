import Axios from "axios";
import { ORDERS_FETCH } from "./types";

export const ordersFetch = () => {
  return dispatch => {
    Axios.get("http://localhost:5000/orders").then(
      res => {
        dispatch({ type : ORDERS_FETCH, payload : res.data })
      }
    )
  }
}

export const orderDelete = id => {
  return dispatch => {
    Axios.delete("http://localhost:5000/orders/" + id).then(res => {
      Axios.get("http://localhost:5000/orders").then(
        res => {
          dispatch({ type : ORDERS_FETCH, payload : res.data })
        }
      )
    })
  }
}