import React, { Component } from 'react';
import Header from '../components/Header';
import Footer from '../components/Footer';

const About = () => {
  return (
    <div>
      <Header />
      <div className="container col-md-5">
        <h3>Header</h3>
        <p className="title text-justify mt-4 mb-3">
          content
        </p>
        <h4 className="text-success">SNAPLOGY</h4>
      </div>
      <Footer company="SNAPLOGY" email="snaplogy@gmail.com" />
    </div>
  )
}

export default About;