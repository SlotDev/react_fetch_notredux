import React, { Component } from 'react';
import Header from "../../components/Header";
import Footer from "../../components/Footer";
import Axios from "axios";
import ProductList from "../../components/product/ProductList";
import { withRouter } from "react-router-dom";

class Product extends Component {

  constructor(props) {
    super(props)
    this.state = { products: null }
    this.delProduct = this.delProduct.bind(this);
    this.editProduct = this.editProduct.bind(this);
  } 

  componentDidMount() {
    Axios.get("http://localhost:5000/products").then( res => {
      this.setState({products: res.data})
    })
  }

  editProduct(products) {
    console.log(products)
    this.props.history.push('products/edit/' + products.id)
  }

  delProduct(products) {
    console.log(products)
    Axios.delete("http://localhost:5000/products/" + products.id).then( res => {
      Axios.get("http://localhost:5000/products").then( 
        res => {
          this.setState({products: res.data})
        }
      )
    })
  }

  render() {
    return(
      <div>
        <Header />
        <div className="container-fluid">
          <div className="row">
            <div className="col-md-6">
              <h1>Products</h1> 
            </div>
            <div className="col-md-6">
              <button className="btn btn-success title float-right" onClick={() => this.props.history.push('products/add')}>Add</button>
            </div>
          </div>
          <ProductList products={this.state.products} onDelProduct={this.delProduct} onEditProduct={this.editProduct} /> 
        </div>
        <Footer />
      </div>
    )
  }

}

export default withRouter(Product);
