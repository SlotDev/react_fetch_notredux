import React, { Component } from 'react';
import Header from '../components/Header';
import Monitor from '../components/monitor/Monitor';
import Footer from '../components/Footer';
import Axios from 'axios';

class Home extends Component {

  constructor(props) {
    super(props)
    this.state = {products : ""}
  }

  componentDidMount() {
    /* 1 */
    /*
    this.setState({products : [
      { id: 1, productName: "MONITOR ASUS PB27UQ 27", unitPrice: "120", thumbnail: "/images/product/MONITOR_ASUS.jpg" },
      { id: 2, productName: "HP ENVY X360 13-AGOOOOAU TOUCH-SCREEN", unitPrice: "90", thumbnail: "/images/product/HPENV.png" },
      { id: 3, productName: "PRINTER HP DESKJET 1112", unitPrice: "200", thumbnail: "/images/product/PRINTER.png" },
      { id: 4, productName: "NAS SYNOLOGY 2-BAY INTEL CELERON", unitPrice: "140", thumbnail: "/images/product/NAS.png" },
      { id: 5, productName: "HEADSET STEELSERIES ARCTIS 3 BLACK 2019", unitPrice: "200", thumbnail: "/images/product/HEADSET.png" },
      { id: 6, productName: "CPU AMD AM4 RYZEN7 2700X 3.7 GHz", unitPrice: "140", thumbnail: "/images/product/CPUAMD.png" }
    ]})
    */

   /* 2 */
   /*
   fetch("http://localhost:5000/products", { method : "GET" })
   .then(res => res.json())
    //  .then(res => { console.log(res) })
   .then(res => { this.setState({ products : res })})
  */

  /* 3 */
  Axios.get("http://localhost:5000/products").then(res => {
    console.log(res.data)
    this.setState({ products : res.data })
    // { this.setState({ products : res.data }) }
  })


}

  render() {
    return (
      <div>
        <Header />        
        <Monitor products={this.state.products} />
        <Footer company="SNAPLOGY" email="snaplogy@gmail.com" />
      </div>
    );
  }
}

export default Home;
